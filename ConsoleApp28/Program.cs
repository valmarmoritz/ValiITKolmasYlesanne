﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// Kaardipakk ja jaotamine 
/// </summary>
/*
Tee enum Mast ja Kõrgus 
Tee strukt Kaart (Mast, Kõrgus) 
Tee kaardipakk (52 kaarti) 
 
Jaga see nelja mängija vahel (Nora, Edda, Sofie, Wendy) 
Tee kindlaks, kellel on kõige tugevam "käsi", selleks 
Leia iga mängija kõige tugevam mast 
Kelle kõige tugevam mast on kõige suurem, sellel on kõige tugevam käsi 
Kui kahel on sama tugev käsi, siis tugevam on tugevam mast (Risti=1 - Poti=4) 
Mõtle, kuidas mõõta kaartide tugevust (näiteks "kaks" = 2 ja "Äss"=14) - Enumi tegemisel vali kõige mugavam viis
 */

namespace ConsoleApp28
{
    enum Mast { Risti, Ärtu, Ruutu, Poti};
    enum Kõrgus {
        Kaks = 2,
        Kolm, Neli, Viis, Kuus, Seitse, Kaheksa, Üheksa, Kümme, Soldat, Emand, Kuningas, Äss };
    
    class Program
    {
        static Kaart[] KaardiPakk;
        static Dictionary<string, Kaart[]> jaotus = new Dictionary<string, Kaart[]>();

        static void Main(string[] args)
        {
            Algandmed();

            Dictionary<string, int> tugevus = new Dictionary<string, int>();
            Dictionary<string, Mast> tugevamMast = new Dictionary<string, Mast>();

            foreach (var x in jaotus)
            {
                Dictionary<Mast, int> rida = new Dictionary<Mast, int>();
                for (Mast m = Mast.Risti; (int)m < 5; m++)
                {
                    rida.Add(m, 0);
                }
                foreach (var y in x.Value)
                {
                    rida[y.mast] += (int)(y.kõrgus);
                }
                Mast tugevMast = 0;
                int tugev = 0;
                foreach (var y in rida)
                {
                    if (y.Value > tugev)
                    {
                        tugev = y.Value;
                        tugevMast = y.Key;
                    }
                }
                tugevus.Add(x.Key, tugev);
                tugevamMast.Add(x.Key, tugevMast);
            }

            string võidab = "";
            Mast võidumast = 0;
            int võidukäsi = 0;

            Console.WriteLine();
            foreach (var x in tugevus)
            {
                Console.WriteLine($"{x.Key} käes on {tugevamMast[x.Key]} rida tugevusega {x.Value}");
                if (x.Value > võidukäsi || (x.Value == võidukäsi && tugevamMast[x.Key] > võidumast) )
                {
                    võidab = x.Key;
                    võidumast = tugevamMast[x.Key];
                    võidukäsi = x.Value;
                }
            }
            Console.WriteLine($"\n{võidab} võidab {võidumast} reaga tugevusega {võidukäsi}!\n");
        }

        static void Algandmed()
        {
            Console.WriteLine("Kaardipaki avamine");
            KaardiPakk = Kaart.AnnaPakk();
            // Kaart.TrükiPakk(KaardiPakk);

            Console.WriteLine("\nKaardipaki segamine");
            KaardiPakk = Kaart.SegaPakk(KaardiPakk);
            // Kaart.TrükiPakk(KaardiPakk);

            string[] mängijad = { "Nora", "Edda", "Susan", "Wendy" };
            int iNimi = 0;
            foreach (var nimi in mängijad)
            {
                jaotus.Add(nimi, new Kaart[13]);
                for (int i = 0; i < 13; i++)
                {
                    jaotus[nimi][iNimi % 13] = KaardiPakk[iNimi++];
                }
            }

            foreach (var x in jaotus)
            {
                Console.Write($"\nMängija {x.Key} kaardid on: ");
                foreach ( var y in x.Value) Console.Write($"{y} ");
                Console.WriteLine();
            }
        }
    }
    struct Kaart
    {
        static Random r = new Random();

        public static Kaart[] AnnaPakk()
        {
            Kaart[] pakk = new Kaart[52];
            for (int i = 0; i<52; i++)
            {
                pakk[i] = new Kaart((Mast)(i / 13), (Kõrgus)(i % 13 + 2));
            }
            return pakk;
        }

        public static Kaart[] SegaPakk(Kaart[] pakk)
        {
            List<Kaart> list = pakk.ToList();
            Kaart[] uusPakk = new Kaart[pakk.Length];
            for (int i = 0; i<pakk.Length; i++)
            {
                int j = r.Next() % list.Count;
                uusPakk[i] = list[j];
                list.Remove(list[j]);
            }
            return uusPakk;
        }

        public static void TrükiPakk(Kaart[] pakk)
        {
            foreach (var x in pakk) Console.WriteLine(x);
        }

        public Mast mast;
        public Kõrgus kõrgus;

        public Kaart (Mast mast, Kõrgus kõrgus)
        {
            this.mast = mast;
            this.kõrgus = kõrgus;
        }

        public override string ToString()
        {
            return $"({this.mast} {this.kõrgus})";
        }
    }
}
